import { useEffect, useReducer } from 'react';
import { 
    BrowserRouter, 
    Redirect, 
    Route
} from 'react-router-dom'

import { Context } from './stores/store';

import indexReducer, { initialState } from './reducers/'
import BookingFlow from './components/BookingFlow/BookingFlow';
import Wrapper from './components/Layout/Wrapper';
import Confirmed from './components/Confirmed/Confirmed'

import Signin from './components/Admin/Signin';
import Scheduler from './components/Admin/Scheduler';

function App() {
    const [store, dispatch] = useReducer(indexReducer, initialState);

    return (
        <Context.Provider value={{ store, dispatch }}>
            <BrowserRouter basename="/app" className="" id="recaptcha-container">
                <Wrapper>
                    <Route exact path="/" component={BookingFlow} />
                    <Route exact path="/admin" component={Signin} />
                    <Route exact path="/confirmed" component={Confirmed} />
                    <Route exact path="/scheduler">
                        {store.auth?.isAuthenticated ? <Scheduler /> : <Redirect to="/" />}
                    </Route>
                </Wrapper>
            </BrowserRouter>
        </Context.Provider>
    );
}

export default App;
