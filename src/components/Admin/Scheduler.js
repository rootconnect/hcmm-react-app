import React, { useEffect, useState } from 'react'
import { useToasts } from 'react-toast-notifications';
import { DateRangePicker } from 'react-dates'
import moment from 'moment'
import $ from 'jquery'

import * as firebase from '../../firebase/firebase'
import { apiCall } from '../../utils/ApiUtils'

const DAY_OF_WEEK_SORT = {
    mon: 1,
    tue: 2,
    wed: 3,
    thu: 4,
    fri: 5,
    sat: 6,
    sun: 7
}

function Scheduler(){
    const { addToast } = useToasts()

    const [state, setState] = useState({
        startDate: moment(),
        endDate: moment().clone(),
        focusedInput: "sDate",
        disableSubmit: false,
        operationHours: {}
    })

    useEffect(() => {
        firebase.database.ref('/operationHours').once('value')
        .then(snapshot => {
            let hours = snapshot.val()

            setState(prev => ({
                ...prev,
                operationHours: hours
            }))
        })
    }, [])

    const bookTimeOff = async() => {
        setState(prev => ({
            ...prev,
            disableSubmit: true
        }))
        const { response } = await apiCall(`booking/timeoff`, {
            method: 'POST',
            body: {
                startDate: state.startDate.format("YYYY-MM-DD"),
                endDate: state.endDate.format("YYYY-MM-DD")
            }
        })

        if(response){
            addToast(`Time off booked from ${state.startDate.format("YYYY-MM-DD")} to ${state.endDate.format("YYYY-MM-DD")}.`, {
                appearance: 'success',
                placement: "top-center",
                autoDismiss: true
            })

        } else {
            addToast("Something went wrong.", {
                appearance: 'error',
                placement: "top-center",
                autoDismiss: true
            })
        }

        setState(prev => ({
            ...prev,
            disableSubmit: false
        }))
    }

    return <div>
        <div className="container-fluid">
            <div className="row">
                <div className="col-lg-12">
                    <div className="breadcrumb-area style-03">
                        <div className="breadcrumb-content">
                            <h1 className="page-title">Scheduler</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div><br />
        <div className="container">
            <DateRangePicker
                startDate={state.startDate}
                startDateId="startDate"
                endDate={state.endDate}
                minimumNights={0}
                endDateId="endDate"
                onDatesChange={({ startDate, endDate }) => setState(prev => ({...prev, startDate, endDate }))}
                focusedInput={state.focusedInput}
                onFocusChange={focusedInput => setState(prev => ({...prev, focusedInput }))}
            />
            <button className="btn btn-primary" disabled={state.disableSubmit} onClick={() => bookTimeOff()}>Book Time Off</button>
        </div>
        <div className="container mt-5">
            <h4 className="mb-3">Toggle Day of Week</h4>
             <div className="row">
                {Object.keys(state.operationHours)
                .sort((a, b) => DAY_OF_WEEK_SORT[a] - DAY_OF_WEEK_SORT[b] )
                .map(key => <div className="col text-center" key={key}>
                    <label className="control-label" htmlFor={`${key}Switch`}>{moment(key, "ddd").clone().format("dddd")}</label>
                    <input type="checkbox"
                        className="bootstrapToggle"
                        id={`${key}Switch`} data-toggle="toggle"
                        onChange={async(e) => {
                            const enabled = e.target.checked

                            await firebase.database.ref(`operationHours`)
                                .child(key)
                                .child('enabled')
                                .set(enabled)

                            await setState(prev => ({
                                ...prev,
                                operationHours: {
                                    ...prev.operationHours,
                                    [key]: {
                                        ...prev.operationHours[key],
                                        enabled
                                    }
                                }
                            }))
                        }}
                        {...({checked: state.operationHours[key].enabled ? "checked" : false})} />
                </div>)}
             </div>
        </div>
    </div>
}

export default Scheduler