import React, { useContext, useState } from 'react'
import { useToasts } from 'react-toast-notifications';
import { useHistory } from 'react-router-dom'

import * as firebase from '../../firebase/firebase'
import * as actionTypes from '../../constants/ActionTypes'

import { Context } from '../../stores/store';

function Signin(){
    const history = useHistory()
    const { addToast } = useToasts()

    const { store, dispatch } = useContext(Context)
    const [state, setState] = useState({
        email: "",
        password: ""
    })

    const initSignin = async() => {
        try {

            const u = await firebase.auth.signInWithEmailAndPassword(state.email, state.password)

            dispatch({
                type: actionTypes.LOGIN,
                email: state.email,
                token: u.user.getIdToken()
            })

            history.push("scheduler")
        } catch(e) {
            addToast("Login email/password is incorrect", {
                appearance: 'error',
                placement: "top-center",
                autoDismiss: false
            })
        }
    }

    return <div>        
        <div className="container-fluid">
            <div className="row">
                <div className="col-lg-12">
                    <div className="breadcrumb-area style-03">
                        <div className="breadcrumb-content">
                            <h1 className="page-title">Sign In</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div><br />
        <div className="container">
            <div className="form">
                <div class="form-group">
                    <label className="control-label">Email</label>
                    <input className="form-control" name="email" type="email" value={state.email} onChange={e => setState(prev => ({
                        ...prev,
                        [e.target.name]: e.target.value
                    }))} />
                </div>
                <div class="form-group">
                    <label className="control-label">Password</label>
                    <input className="form-control" name="password" type="password" value={state.password} onChange={e => setState(prev => ({
                        ...prev,
                        [e.target.name]: e.target.value
                    }))} />
                </div>
                <button className="btn btn-primary" onClick={() => initSignin()}>Sign In</button>
            </div>
        </div>
    </div>
}

export default Signin