import React, { useContext, useEffect, useState } from 'react'
import moment from 'moment'

import Service from '../Service/Service'
import { Context } from '../../stores/store';

import { ADD_SERVICE, MODIFY_CONTACT_DETAILS, REMOVE_SERVICE, RESET_BOOKING_TIME, SELECT_SERVICE, SET_BOOKING_DATE, SET_BOOKING_TIME, SET_PAGE } from '../../constants/ActionTypes'
import BookingCalendar from '../Calendar/BookingCalendar';
import { apiCall } from '../../utils/ApiUtils';
import { serviceInitialState } from '../../reducers/service';
import LocationSearchInput from '../Places/LocationSearchInput';

import marker from '../../assets/img/marker.png'
import { cartInitialState } from '../../reducers/cart';
import { errorMessages } from '../../constants/ErrorMessages';

const BUFFER = 1
const COST = 50

const STEP_INFO = [
    { 
        title: "Choose your Services", 
        description: `Please select the services that are needed for your vehicle below! If you do not see your service listed:<br /> <div class="main-btn-wrap padding-top-10"><a href="tel:${process.env.REACT_APP_PHONE}" class="main-btn black">Give us a Call!</a></div>`},
    {
        title: "Add your Info",
        description: `Please fill out your information below including location of service, vehicle details, and more! <br /> <br /><div class="basic-alert-box red-border mx-auto d-block" style="max-width:400px">
        <div class="basic-alert__inner">
            <strong><p>All fields are required.</p></strong>
        </div>
    </div>`
    },
    {
        title: "Book your Timeslot",
        description: "The calendar below will have available dates and slots for you to choose from"
    },
    {
        title: "Confirm your Booking",
        description: `Confirm your details, sit back and let us come to you!`
    }
]


function BookingFlow(){
    const { store, dispatch } = useContext(Context)
    const { cart } = store

    const [location, setLocation] = useState("")
    const [timeslots, setTimeslots] = useState([])
    const [toggleCalendar, setToggleCalendar] = useState(false)
    const [totalCost, setTotalCost] = useState(0)
    const [modal, modalOpen] =useState(false)

    const selectService = (service) => {
        if(cart.services[service.id]){
            dispatch({
                type: REMOVE_SERVICE,
                serviceId: service.id
            })
        } else {
            if(service.options){
                dispatch({
                    type: SELECT_SERVICE,
                    service
                })
            } else {
                dispatch({
                    type: ADD_SERVICE,
                    service
                })                
            }
        }
    }

    const changeValue = (e) => {
        let contactDetails = cart.contactDetails
        contactDetails[e.target.name] = e.target.value
        dispatch({
            type: MODIFY_CONTACT_DETAILS,
            contactDetails
        })
    }

    const getAvailability = async(date) => {
        if(cart.currentStep === 3){
            const { services } = cart
            let totalHours = BUFFER

            setTimeslots([])

            Object.values(services).forEach(s => {
                if(s.timeEstimate){
                    totalHours += s.timeEstimate
                } else {
                    Object.values(s.optionsSelected).forEach(o => {
                        totalHours += o.timeEstimate
                    })
                }
            });

            let currenttime = moment().local(false).format("HH:mm:ssZ")
            const { response, error } = await apiCall('booking/availability', {
                method: "POST",
                body: {
                    scheduledHours: totalHours,
                    date: date.format("YYYY-MM-DD") + "T" + currenttime
                }
            })
            if(response){
                setTimeslots(response.timeslots.length > 0 ? response.timeslots : null)
            } else {
                setTimeslots(null)
            }

            dispatch({
                type: SET_BOOKING_DATE,
                date: date.format("YYYY-MM-DD") + "T" + currenttime
            })
            dispatch({
                type: RESET_BOOKING_TIME
            })
        }
    }

    const preFillValues = () => {
        if(process.env.NODE_ENV === "development"){
            let contactDetails = {
                ...cart.contactDetails,
                firstName: "Dan",
                lastName: "Don",
                email: "d_madon@mailinator.com",
                phone: "9998887777",
                make: "Honda",
                model: "Civic",
                color: "Red",
                serviceLocation: {
                    description: "123 Main St E, Hamilton ON"
                }
            }
            dispatch({
                type: MODIFY_CONTACT_DETAILS,
                contactDetails
            })

        }
    }

    const setBookingTime = (slot) => {
        dispatch({
            type: SET_BOOKING_TIME,
            slot
        })
    }

    useEffect(() => {
        let contactDetails = cart.contactDetails

        if(location === ""){
            contactDetails.serviceLocation = cartInitialState.contactDetails.serviceLocation
        } else {
            contactDetails.serviceLocation = {
                description: location
            }
        }
        dispatch({
            type: MODIFY_CONTACT_DETAILS,
            contactDetails
        })
    },[location])

    useEffect(() => {
        if(cart.booking.date === cartInitialState.booking.date){
            getAvailability(moment());
        } else {
            if(cart.currentStep === 4){
                const { services } = cart
                let totalHours = BUFFER

                Object.values(services).forEach(s => {
                    if(s.timeEstimate){
                        totalHours += s.timeEstimate
                    } else {
                        Object.values(s.optionsSelected).forEach(o => {
                            totalHours += o.timeEstimate
                        })
                    }
                }); 
                setTotalCost(COST*totalHours)
            }
        }
    }, [cart])

    const currentStep = STEP_INFO[cart.currentStep - 1]

    return <div>
        <div className="container-fluid">
            <div className="row">
                <div className="col-lg-12">
                    <div className="breadcrumb-area style-03" id="bookingTitle">
                        <div className="breadcrumb-content">
                            <h1 className="page-title">{currentStep.title}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="text-center padding-top-20">
            <h5 dangerouslySetInnerHTML={({ __html: currentStep.description})}></h5>
        </div>
        <section className="container padding-top-40">
            {cart.currentStep === 1 && <div className="row">
                <div className="col-lg-12">
                    <div className="car-filter-content">
                        <div className="car-filter-content-active">
                            {store.service.servicesLoaded ? store.service.items.map(s => {
                                let added = false
                                
                                if(cart.services[s.id]){
                                    added = true
                                }
                                return <Service {...s} onClick={() => s.callOnly ? window.location.href=`tel:${process.env.REACT_APP_PHONE}` : selectService(s)} added={added} />
                            }): ''}
                        </div>
                    </div>
                </div>
            </div>}
            {cart.currentStep === 2 && <>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="step-car-sell__details">
                            <form className="needs-validation" noValidate>
                                <div className="step-car-sell__details_form style-01">
                                    <div className="form-row">
                                        <div className="col-lg-6 mb-lg-4 mb-4">
                                            <label for="first_name" onDoubleClick={() => preFillValues()}>First Name</label>
                                            <input id="first_name" name="firstName" type="text" className="form-control" onChange={changeValue} value={cart.contactDetails.firstName} />
                                        </div>
                                        <div className="col-lg-6 mb-lg-4 mb-4">
                                            <label for="last_name">Last Name</label>
                                            <input id="last_name" name="lastName" type="text" className="form-control" onChange={changeValue} value={cart.contactDetails.lastName} />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-lg-6 mb-lg-4 mb-4">
                                            <label for="email_address">Email Address</label>
                                            <input id="email_address" name="email" type="email" className="form-control" onChange={changeValue} value={cart.contactDetails.email} />
                                            {errorMessages.email(cart.contactDetails.email) && <div class="invalid-feedback">
                                                {errorMessages.email(cart.contactDetails.email)}
                                            </div>}
                                        </div>
                                        <div className="col-lg-6 mb-lg-4 mb-4">
                                            <label for="phone_number">Phone Number</label>
                                            <input id="phone_number" name="phone" type="text" className="form-control" onChange={changeValue} value={cart.contactDetails.phone} />
                                            {errorMessages.phone(cart.contactDetails.phone) && <div class="invalid-feedback">
                                                {errorMessages.phone(cart.contactDetails.phone)}
                                            </div>}
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-lg-4 mb-lg-4 mb-4">
                                            <label for="vehicle_make">Vehicle Make</label>
                                            <input id="vehicle_make" type="text" name="make" className="form-control" onChange={changeValue} value={cart.contactDetails.make} />
                                        </div>
                                        <div className="col-lg-4 mb-lg-4 mb-4">
                                            <label for="vehicle_model">Model</label>
                                            <input id="vehicle_model" type="text" name="model" className="form-control" onChange={changeValue} value={cart.contactDetails.model} />
                                        </div>
                                        <div className="col-lg-4 mb-lg-4 mb-4">
                                            <label for="color">Vehicle Color</label>
                                            <input id="color" type="text" name="color" className="form-control" onChange={changeValue} value={cart.contactDetails.color} />
                                        </div>
                                    </div>
                                    <div className="form-row mb-lg-4 mb-4">
                                        <div className="col-lg-6">
                                            <label for="location">Where Will We Service Your Vehicle? (Full Address Including City) <button type="button" className="btn btn-sm btn-outline-dark" data-toggle="modal" data-target="#mapOfAreaModal">
                                                <i className="fa fa-exclamation-circle"></i> Service Area
                                            </button></label>
                                            <input type="text" className="form-control" name="location" value={location} onChange={e => setLocation(e.target.value)} />
                                        </div>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div> 
                </div>

                <div class="modal fade" id="mapOfAreaModal" tabindex="-1" role="dialog" aria-labelledby="mapOfAreaTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mapOfAreaTitle">Map of Service Area</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <img className="img img-responsive img-fluid" src="https://hamiltonmobilemechanic.com/images/map.jpg"></img>
                            </div>
                        </div>
                    </div>
                </div>
            </>}
            {cart.currentStep === 3 && <div className="row">
                <div className="col-lg-4 col-md-6 col-sm-12 d-sm-none d-none d-md-block">
                    <BookingCalendar onDateChange={getAvailability} />
                </div>
                {!toggleCalendar &&<div className="col-sm-12 d-md-none d-sm-block d-block">
                    <BookingCalendar onDateChange={(date) => {
                        setToggleCalendar(true)
                        getAvailability(date);
                    }} />
                </div>}
                {timeslots === null ? <div className="col-lg-8 col-md-6 d-md-block d-sm-none d-none">
                        <h5 className="text-center text-danger">There are no timeslots available for {moment(cart.booking.date, moment.ISO_8601).format("dddd, MMMM Do")}</h5>
                    </div> : timeslots !== null && timeslots.length > 0 && <div className="col-lg-8 col-md-6 d-md-block d-sm-none d-none">
                    <div className="row">
                        {timeslots.map(slot => <div className="col-lg-3 col-md-6">
                            <button className={`btn btn-lg btn-block ${cart.booking.slot.startTime === slot.startTime ? 'btn-success': 'btn-primary'}`} onClick={() => setBookingTime(slot)} style={{marginBottom: "10px"}}>
                                {slot.startTime} to {slot.endTime}
                            </button>
                        </div>)}
                    </div>
                </div>}
                {timeslots === null && toggleCalendar ? <div className="col-sm-12 col-12 d-md-none d-sm-block d-block">
                    <div className="row">
                        <div className="col-sm-12">
                            <button className="btn btn-outline-primary" onClick={() => setToggleCalendar(false)} style={{marginBottom: "10px", borderRadius: "20px"}}>
                                <i className="fas fa-arrow-left"></i>
                            </button>
                        </div>
                    </div>
                    <h5 className="text-center text-danger">There are no timeslots available for {moment(cart.booking.date, moment.ISO_8601).format("dddd, MMMM Do")}</h5>
                </div> : timeslots !== null && timeslots.length > 0 && toggleCalendar && <div className="col-sm-12 col-12 d-md-none d-sm-block d-block">
                    <div className="row">
                        <div className="col-sm-12">
                            <button className="btn btn-outline-primary" onClick={() => setToggleCalendar(false)} style={{marginBottom: "10px", borderRadius: "20px"}}>
                                <i className="fas fa-arrow-left"></i>
                            </button>
                        </div>
                    </div>
                    <div className="row">
                        {timeslots.map(slot => <div className="col-sm-12">
                            <button className={`btn btn-lg btn-block ${cart.booking.slot.startTime === slot.startTime ? 'btn-success': 'btn-primary'}`} onClick={() => setBookingTime(slot)} style={{marginBottom: "10px"}}>
                                {slot.startTime} to {slot.endTime}
                            </button>
                        </div>)}
                    </div>
                </div>}
                
            </div>}
            {cart.currentStep === 4 && <>
            <p>
                <strong>
                    <em>
                        By confirming this booking, you agree to have the vehicle parts necessary (if any) are available to the mechanic no later than {moment(cart.booking.date, moment.ISO_8601).format("dddd, MMMM Do YYYY")} at {cart.booking.slot.startTime}.
                    </em>
                </strong>
            </p>
            <div className="row">
                <div className="col-lg-4 col-md-12">
                    <div className="widget-area">

                        <div className="widget blog-bg style-01">
                            <h5 className="widget-title border-bottom">
                                Your Quote
                                <button onClick={() => dispatch({
                                    type: SET_PAGE,
                                    step: 1
                                })}>
                                    <i className="fa fa-pen"></i>
                                </button>
                            </h5>
                            <ul className="recent_post_item">
                                {Object.values(cart.services).map(service => {
                                    let totalPrice = 0
                                    let totalHours = 0
                                    if(service.optionsSelected){
                                        Object.values(service.optionsSelected).forEach(o => {
                                            if(o.timeEstimate){
                                                totalPrice += (COST * o.timeEstimate)
                                                totalHours += o.timeEstimate
                                            }
                                        })
                                    } else if(service.timeEstimate){
                                        totalPrice = COST * service.timeEstimate
                                        totalHours = service.timeEstimate
                                    }
                                    return <li className="single-recent-post-item">
                                        <div className="content">
                                            <h5 className="title"><a href="#">{service.name}</a></h5>
                                            <div className="left">
                                                Estimate: <span className="black">{totalHours} Hours</span>
                                            </div>
                                            <div className="common-price-style">
                                                Price: <span className="black">${totalPrice.toFixed(2)}</span>
                                            </div>
                                        </div>
                                    </li>
                                })}
                                <li className="single-recent-post-item">
                                    <div className="content">
                                        <h5 className="title"><a href="#">Booking Fee</a></h5>
                                        <div className="common-price-style">
                                            Price: <span className="black">${COST.toFixed(2)}</span>
                                        </div>
                                    </div>
                                </li>
                                
                            </ul>
                            <h5 className="widget-title border-top margin-top-20 padding-top-10">
                                ${totalCost.toFixed(2)}
                                <h6 style={{
                                    float: "right",
                                    fontSize: "0.5em"
                                }}>Plus applicable taxes</h6>
                            </h5>
                            <h6>* Please note that this quote value is just the starting price. The price may increase based on the work needed</h6>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4 col-md-6">
                    <div className="widget-area padding-bottom-40 mt-2 mt-lg-0">
                        <div className="widget blog-bg style-01">
                            <h5 className="widget-title border-bottom">
                                Details
                                <button onClick={() => dispatch({
                                    type: SET_PAGE,
                                    step: 2
                                })}>
                                    <i className="fa fa-pen"></i>
                                </button>
                            </h5>
                            <div class="basic-alert-box">
                                <div class="basic-alert__inner">
                                    <h5><i class="basic-alert__icon flaticon-small-calendar"></i> {moment(cart.booking.date, moment.ISO_8601).format("dddd, MMMM Do YYYY")} at {cart.booking.slot.startTime}</h5>
                                </div>
                            </div>
                            <table className="table table-borderless">
                                <tbody className="shopping-cart__body">
                                    <tr className="shopping-cart__item">
                                        <td>
                                            <strong>Name</strong>
                                        </td>
                                        <td>
                                            <div className="price">{cart.contactDetails.firstName} {cart.contactDetails.lastName}</div>
                                        </td>
                                    </tr>
                                    <tr className="shopping-cart__item">
                                        <td>
                                            <strong>Email</strong>
                                        </td>
                                        <td>
                                            <div className="price">{cart.contactDetails.email}</div>
                                        </td>
                                    </tr>
                                    <tr className="shopping-cart__item">
                                        <td>
                                            <strong>Phone</strong>
                                        </td>
                                        <td>
                                            <div className="price">{cart.contactDetails.phone}</div>
                                        </td>
                                    </tr>
                                    <tr className="shopping-cart__item">
                                        <td>
                                            <strong>Vehicle</strong>
                                        </td>
                                        <td>
                                            <div className="price">{cart.contactDetails.make} {cart.contactDetails.model}, Color: {cart.contactDetails.color}</div>
                                        </td>
                                    </tr>
                                    <tr className="shopping-cart__item">
                                        <td>
                                            <strong>Vehicle Color</strong>
                                        </td>
                                        <td>
                                            <div className="price">{cart.contactDetails.color}</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>               
                    
                </div>
                <div className="col-lg-4 col-md-6">
                    <div className="quick-find-us mt-3 mt-lg-0">
                        <div className="find-us-inner mx-auto d-block" style={{marginBottom: "auto"}}>
                            <h4 className="title">Service Location</h4>
                            <div className="contact-items padding-top-20">
                                <div className="icon">
                                    <img src={marker} alt="marker" style={{filter: "invert(1)"}} />
                                </div>
                                <h5 className="heading">{cart.contactDetails.serviceLocation?.description}</h5>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div></>}
        </section>
    </div>
}

export default BookingFlow