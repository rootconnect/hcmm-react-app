import React, { useContext, useEffect, useState } from 'react'
import { useHistory } from "react-router-dom";

import { Context } from '../../stores/store'

import { cartInitialState } from '../../reducers/cart'
import { CART_ERROR, NEXT_PAGE, PREVIOUS_PAGE } from '../../constants/ActionTypes'
import { apiCall } from '../../utils/ApiUtils'
import { errorMessages } from '../../constants/ErrorMessages';
import { useToasts } from 'react-toast-notifications';

function BookingTracker(){
    const { store, dispatch } = useContext(Context)

    const [toggleNav, setToggleNav] = useState(false)
    const [nextDisabled, setNextDisabled] = useState(true)
    const [completingBooking, setCompletingBooking] = useState(false)

    const history = useHistory();
    const { addToast } = useToasts()

    const { cart } = store

    const nextButtonDisabled = () => {
        if(cart.currentStep === 1 && Object.keys(cart.services).length === Object.keys(cartInitialState.services).length){
            setNextDisabled(true)
            return
        }

        if(cart.currentStep === 2){
            let disabled = false
            Object.keys(cart.contactDetails).forEach(d => {
                if(cart.contactDetails[d] === "" || cart.contactDetails[d] === null){
                    disabled = true
                    return
                }
            })
            if(errorMessages.email(cart.contactDetails.email)){
                disabled = true
            }
            if(errorMessages.phone(cart.contactDetails.phone)){
                disabled = true
            }
            if(cart.contactDetails.serviceLocation === cartInitialState.contactDetails.serviceLocation || 
                cart.contactDetails.serviceLocation?.description === ""){
                disabled = true
            }
            setNextDisabled(disabled)
            return
        }

        if(cart.currentStep === 3){
            setNextDisabled(cart.booking.date === null || cart.booking.slot.startTime === null)
            return
        }

        setNextDisabled(false)
    }

    const goToPrev = () => {
        dispatch({
            type: PREVIOUS_PAGE
        })
    }

    const goToNext = async() => {
        if(!nextDisabled){
            if(cart.currentStep === 4){
                await setCompletingBooking(true)
                window.grecaptcha.ready(async function() {
                    let recaptchaToken = await window.grecaptcha.execute(process.env.REACT_APP_RECAPTCHA_SITE_KEY, {action: 'submit'})
                    const { response } = await apiCall(`booking`, {
                        method: 'POST',
                        body: {
                            cart,
                            recaptchaToken
                        }
                    })

                    await setCompletingBooking(false)

                    if(response){
                        history.push("/confirmed")
                    } else {
                        window.scrollTo(0, 0)
                        dispatch({
                            type: CART_ERROR
                        })
                        addToast("Something went wrong. Please try again or call us using the phone icon in the nav bar", {
                            appearance: 'error',
                            placement: "top-center",
                            autoDismiss: true
                        })
                    }
                });

            } else {
                window.scrollTo(0, 0)
                dispatch({
                    type: NEXT_PAGE
                })
            }
        }
    }

    useEffect(() => {
        const threshold = 0;
        let lastScrollY = window.pageYOffset;
        let ticking = false;

        const updateScrollDir = () => {
            const scrollY = window.pageYOffset;

            if (Math.abs(scrollY - lastScrollY) < threshold) {
                ticking = false;
                return;
            }
            setToggleNav(nextDisabled && scrollY > lastScrollY ? true : false);
            lastScrollY = scrollY > 0 ? scrollY : 0;
            ticking = false;
        };

        const onScroll = () => {
            if (!ticking) {
                window.requestAnimationFrame(updateScrollDir);
                ticking = true;
            }
        };

        window.addEventListener("scroll", onScroll);

    }, [toggleNav]);

    useEffect(() => {
        (async() => {
            let v = await nextButtonDisabled()
            if(!nextDisabled) {
                setToggleNav(false)
            } else {
                setToggleNav(true)
            }
        })()
    }, [cart, nextDisabled])
    
    return toggleNav ? '' :<div className="border-top-1 copyright-area bottom-nav-fixed">
        {cart.services && Object.keys(cart.services).length > 0 && <button type="button" class="btn btn-danger btn-sm bottom-nav-abs1">
            <i class="fas fa-tools"></i> <span class="badge badge-light">{Object.keys(cart.services).length}</span>
        </button>}
        <div className="container d-flex" style={{justifyContent: "space-between"}}>
            <div class="main-btn-wrap text-center">
                {cart.currentStep !== 1 && <button class={`main-btn black-border font-weight-bold`} disabled={completingBooking} onClick={() => goToPrev()}>PREVIOUS</button>}
            </div>
            <div class="main-btn-wrap text-center">
                <button class={`main-btn font-weight-bold ${nextDisabled || completingBooking ? 'black-border':'black pulse'}`} disabled={nextDisabled || completingBooking} onClick={() => goToNext()}>
                    {cart.currentStep === 3 ? "REVIEW" : cart.currentStep === 4 ? "BOOK" : "CONTINUE"}
                    {completingBooking && <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>}
                </button>
            </div>
        </div>
    </div>
}

export default BookingTracker