import React, { useContext } from 'react'
import { DayPickerSingleDateController } from 'react-dates'
import moment from 'moment'

import { Context } from '../../stores/store';
import { cartInitialState } from '../../reducers/cart';

function BookingCalendar(props){
    const { store } = useContext(Context)
    const { cart } = store

    let now = moment()

    return <DayPickerSingleDateController
        numberOfMonths={1}
        date={cart.booking.date ? moment(cart.booking.date) : cartInitialState.booking.date}
        onDateChange={date => {
            props.onDateChange(date)
        }}
        isOutsideRange={(dt) => {
            if(dt.isBefore(now.clone().subtract(1, 'days'))){
                return true
            }
            if(dt.isAfter(now.clone().add(14, 'days'))){
                return true
            }

            return false
        }}
    />
}

BookingCalendar.defaultProps = {
    onDateChange: () =>{}
}

export default BookingCalendar