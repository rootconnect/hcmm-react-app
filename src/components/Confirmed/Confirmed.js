import React, { useContext, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { CLEAR_CART } from '../../constants/ActionTypes'
import { Context } from '../../stores/store';

function Confirmed(){
    const [state, setState] = useState({})
    const { store, dispatch } = useContext(Context)
    const { cart } = store

    useEffect(() => {
        setState(cart)

        dispatch({
            type: CLEAR_CART,
            disabled: true
        })

        return () => dispatch({
            type: CLEAR_CART
        })
    }, [])

    return <div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-area style-03" id="bookingTitle">
                        <div class="breadcrumb-content">
                            <h1 class="page-title">Booking Confirmed!</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="modal-box padding-top-40">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-8 col-sm-12">
                        <div class="modal-box-item text-center tb-margin-60" style={{maxWidth: "fit-content"}}>
                            <div class="modal-box-item__icon green">
                                <i class="flaticon-tick"></i>
                            </div>
                            <div class="modal-box-item__content">
                                <h5 class="heading-05">Congratulations</h5>
                                <p>Your booking request is successful! Please check you email for your confirmation</p>
                            </div>
                            <div class="modal-box-item__button">
                                <div class="main-btn-wrap">
                                    <Link to="/" class="main-btn black uppercase">Home</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default Confirmed