import React, { useContext, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'

import * as firebase from '../../firebase/firebase'
import { ADD_SERVICE, DESELECT_SERVICE, LOGOUT } from '../../constants/ActionTypes'
import logo from '../../hmm.png'
import { serviceInitialState } from '../../reducers/service'
import { Context } from '../../stores/store'
import BookingTracker from '../BookingFlow/BookingTracker'

const INITIAL_STATE = {
    activeClass: "",
    options: {},
    error: false
}

const optionsStatement = {
    many: "Choose one or more of the options listed below",
    one: "Choose one of the options listed below"
}

function Header(){
    const history = useHistory()
    const [state, setState] = useState(INITIAL_STATE)
    const { store, dispatch } = useContext(Context)

    const { service, cart, auth } = store

    useEffect(() => {
        let activeClass = service.selected !== serviceInitialState.selected ? "active" : ""
        let options = INITIAL_STATE.options

        if(service.selected.id && cart?.services[service.selected.id]?.options){
            options = cart.services[service.selected.id].options
        }

        setState(prevState => ({
            ...prevState,
            activeClass,
            options
        }))
    }, [service, cart])

    const logoutUser = async() => {
        await firebase.auth.signOut()

        dispatch({
            type: LOGOUT
        })

        history.push("admin")
    }

    const validateServiceAdd = () => {
        const { options } = state
        let error = false
        if(service.selected.requiredOptions || service.selected.optionsType === "one"){
            if(options){
                let count = 0
                Object.values(options).forEach(option => {
                    if(option.value){
                        count ++
                    }
                })

                if((count < service.selected.requiredOptions) || (service.selected.optionsType === "one" && count < 1)){
                    error = `You must choose at least ${service.selected.requiredOptions || 1} options before proceeding`
                }
            } else {
                error = `You must choose at least ${service.selected.requiredOptions || 1} options before proceeding`
            }
        }

        if(error){
            setState(prevState => ({
                ...prevState,
                error
            }))
        } else {
            let optionsSelected = {}
            Object.keys(options).forEach(key => {
                if(options[key].value){
                    optionsSelected[key] = options[key]
                }
            })

            service.selected.optionsSelected = optionsSelected
            dispatch({
                type: ADD_SERVICE,
                service: service.selected
            })

            dispatch({
                type: DESELECT_SERVICE
            })

        }

    }

    const { options, activeClass } = state

    return <>
        <div class="preloader" id="preloader">
            <div class="preloader-inner">
                <div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                </div>
            </div>
        </div>
        {!cart.disabled && !auth.isAuthenticated && <BookingTracker />}

        <div id="sidebar" className={"sidebar-class right "+activeClass}>
            <div class="toggle-btn" onClick={() => {
                setTimeout(() => dispatch({
                    type: DESELECT_SERVICE
                }), 1000); 
                setState(prevState => ({
                    ...prevState,
                    error: INITIAL_STATE.error
                }))
            }}>
                <span class="closebtn" title="Close Overlay">×</span>
            </div>

            <div class="sidebar-contact-area">
                <div class="section-title padding-bottom-20">
                    <h4 class="heading-04">{service.selected.name}</h4>
                </div>
                <div class="timing-area">
                    <form>
                        {state.error && <p className="alert alert-danger">{state.error}</p>}
                        <p className="text-white">
                            {service.selected.label ? service.selected.label : optionsStatement[service.selected.optionsType] ? optionsStatement[service.selected.optionsType] : ""}
                        </p>
                        {service.selected.optionsType !== "one" && service.selected.options && Object.keys(service.selected.options).map((key) => (<>
                            <div className="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id={key} checked={options[key]?.value ? "checked" : false} onChange={(e) => setState(prevState =>({
                                        ...prevState,
                                        options: {
                                            ...prevState.options,
                                            [key]: {
                                                ...service.selected.options[key],
                                                value: prevState.options[key] && prevState.options[key].value ? false : true
                                            }
                                        }
                                    }))} />
                                    <label class="custom-control-label text-white" htmlFor={key}>{service.selected.options[key].name}</label>
                                </div>
                            </div>
                        </>))}
                        {service.selected.optionsType === "one" && service.selected.options && 
                            Object.values(service.selected.options)[0].type === "textarea" ?
                            Object.keys(service.selected.options).map((key, idx) => (<>
                                    <label htmlFor={key} className="control-label text-white">{service.selected.options[key].name}</label>
                                    <textarea class="form-control" id={key} rows="3" onChange={(e) => setState(prevState =>({
                                        ...prevState,
                                        options: {
                                            ...prevState.options,
                                            [key]: {
                                                ...service.selected.options[key],
                                                value: e.target.value
                                            }
                                        }
                                    }))}></textarea>
                                </> 
                            )) : service.selected.optionsType === "one" ? <div class="btn-group btn-group-toggle" data-toggle="buttons">{Object.keys(service.selected.options).map((key, idx) => <>
                                <label class="btn btn-outline-light" onClick={(e) => setState(prevState =>({
                                    ...prevState,
                                    options: {
                                        ...prevState.options,
                                        [service.selected.id]: {
                                            ...service.selected.options[key],
                                            value: service.selected.options[key].value
                                        }
                                    }
                                }))}>
                                    <input type="radio" name="options" id={key} autoComplete="off" 
                                        value={service.selected.options[key].value} 
                                        defaultChecked={idx === 0}
                                        // checked={options[service.selected.id]?.value === service.selected.options[key].value} 
                                        /> {service.selected.options[key].name}
                                </label>
                            </>)}</div> : null}
                    </form>
                </div>
                <br />
                <button className="btn btn-lg btn-success" onClick={() => validateServiceAdd()}>Save</button>
            </div>
        </div>
        <div id="menu-cart-open">
            <div class="cart-title-wrap">
                <div class="title">
                    <h3 class="heading-03">Cart</h3>
                </div>
                <div class="show-cart">
                    <span class="closebtn" title="Close Overlay">×</span>
                </div>
            </div>
            <div class="cart-content">
                <div class="cart-item-wrap">
                    <div class="cart-single-item">
                        <div class="item-left">
                            <img src="assets/img/cart/01.png" alt="img" />
                        </div>
                        <div class="item-right">
                            <div class="item-text">
                                <h5 class="heading-05">Honda NSX</h5>
                                <div class="price"><span class="price_text"> Price:</span> $145 </div>
                            </div>
                        </div>
                        <div class="cart-item-close-btn">×</div>
                    </div>
                    <div class="cart-single-item">
                        <div class="item-left">
                            <img src="assets/img/cart/02.png" alt="img" />
                        </div>
                        <div class="item-right">
                            <div class="item-text">
                                <h5 class="heading-05">Auto Clutch & Brake</h5>
                                <div class="price"><span class="price_text"> Price:</span> $145 </div>
                            </div>
                        </div>
                        <div class="cart-item-close-btn">×</div>
                    </div>
                    <div class="cart-single-item">
                        <div class="item-left">
                            <img src="assets/img/cart/03.png" alt="img" />
                        </div>
                        <div class="item-right">
                            <div class="item-text">
                                <h5 class="heading-05">Flash Deals Tyre</h5>
                                <div class="price"><span class="price_text"> Price:</span> $145 </div>
                            </div>
                        </div>
                        <div class="cart-item-close-btn">×</div>
                    </div>

                    <div class="cart-single-item subtotal">
                        <div class="item-left">
                            <h5 class="heading-05">Subtotal:</h5>
                        </div>
                        <div class="item-right">
                            <div class="amount">$45,180</div>
                        </div>
                    </div>

                    <div class="main-btn-wrap padding-top-20">
                        <a href="#" class="main-btn black">VIEW CART</a>
                    </div>
                    <div class="main-btn-wrap padding-top-20">
                        <a href="#" class="main-btn black">CHECKOUT</a>
                    </div>
                </div>
            </div>
        </div>
        <header>
            <div class="container">
                <div class="row">
                    <div class="header-bottom-area">
                        <div class="logo-area">
                            <a href="/app">
                                <img src={logo} alt="Logo" />
                            </a>
                        </div>
                        <nav class="navbar navbar-area navbar-expand-lg style-02">
                            <div class="container nav-container">
                                <div class="collapse navbar-collapse" id="autoshop_main_menu">
                                    <ul class="navbar-nav">
                                        {/* <li class="current-menu-item">
                                            <a href="login">Login</a>
                                            <ul class="sub-menu">
                                                <li><a href="index.html">Home 01</a></li>
                                                <li><a href="index-2.html">Home 02</a></li>
                                                <li><a href="index-3.html">Home 03</a></li>
                                                <li><a href="index-4.html">Home 04</a></li>
                                                <li><a href="index-5.html">Home 05</a></li>
                                            </ul>
                                        </li> */}
                                        {/* <li class="menu-item-has-children">
                                            <a href="#">Services</a>
                                            <ul class="sub-menu">
                                                <li><a href="service.html">Services</a></li>
                                                <li><a href="service-details.html">Services Details</a></li>
                                            </ul>
                                        </li> */}
                                    </ul>
                                </div>
                                {/* <div class="nav-right-content black">
                                    <ul>
                                        <li class="cart show-cart">
                                            <a href="#" class="notification">
                                                <i class="fa fa-shopping-cart"></i>
                                                <span class="badge">3</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div> */}
                                {cart.error && <div class="nav-right-content black">
                                    <ul>
                                        <li>
                                            <a href={`tel:${process.env.REACT_APP_PHONE}`} class="notification">
                                                <i class="fa fa-phone"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>}

                                {!auth?.isAuthenticated && <div class={`nav-right-content`}>
                                    <button className="btn btn-link" onClick={() => history.push("/admin")}>Login</button>
                                </div>}
                                {auth?.isAuthenticated &&  <div class="nav-right-content black">
                                    <button className="btn btn-primary" onClick={() => logoutUser()}>Logout</button>
                                </div>}
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
    </>
}

export default Header