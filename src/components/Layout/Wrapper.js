import React, { useContext, useEffect } from 'react'
import { ToastProvider } from 'react-toast-notifications'
import { useHistory } from 'react-router';

import Header from './Header'

import * as firebase from '../../firebase/firebase'
import { LOAD_SERVICES, LOGIN } from '../../constants/ActionTypes';
import { Context } from '../../stores/store';
function Wrapper(props){
    const { store, dispatch } = useContext(Context)
    const history = useHistory()

    useEffect(() => {
        if(process.env.NODE_ENV === "production"){
            console.log = function() {}
        }
        (async() => {

            const snapshot = await firebase.database.ref('/services').once('value')
            const services = snapshot.val()

            let keySort = Object.keys(services).sort((a, b) => services[b].callOnly && !services[a].callOnly ? -1 : services[a].callOnly && !services[b].callOnly ? 1 : 0)

            const serviceSorted = {}

            keySort.forEach(k => serviceSorted[k] = services[k])
            dispatch({
                type: LOAD_SERVICES,
                services: serviceSorted
            })

            const user = firebase.auth.currentUser;
            if(user && user.email){
                console.log(user)
                dispatch({
                    type: LOGIN,
                    email: firebase.auth.currentUser.email,
                    token: firebase.auth.currentUser.getIdToken()
                })
                history.push("scheduler")
            } else {
                await firebase.auth.signInAnonymously()
            }

        })()
    }, [])


    return <ToastProvider><div style={{marginBottom: "120px"}}>
        <Header />
        <div className="container-fluid">
            {props.children}
        </div>
    </div></ToastProvider>
}

export default Wrapper