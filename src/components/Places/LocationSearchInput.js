import React from 'react';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

function LocationSearchInput({ value, onChange }) {

  /*handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => console.log('Success', latLng))
      .catch(error => console.error('Error', error));
  };*/

    return (
      <PlacesAutocomplete
        value={value}
        onChange={onChange}
        // onSelect={this.handleSelect}
        searchOptions={{
          radius: 5800,
          location: new window.google.maps.LatLng(43.244991, -79.829994),
          types: ['address']
      }}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
            <>
                <div class="input-group">
                    <input
                        {...getInputProps({
                            placeholder: 'Search Places ...',
                            className: 'location-search-input form-control',
                        })}
                        />
                    {value.length > 0 && <div class="input-group-append"><button className="btn btn-outline-danger" onClick={() => onChange("")}>
                        <i class="fas fa-times"></i>
                    </button></div>}
                </div>
                <div className="autocomplete-dropdown-container">
                    {loading ? <div>Loading...</div>: suggestions.map(suggestion => {
                        const className = suggestion.active
                        ? 'suggestion-item--active'
                        : 'suggestion-item';
                        // inline style for demonstration purpose
                        const style = suggestion.active
                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                        return (
                        <div
                            {...getSuggestionItemProps(suggestion, {
                            className,
                            style,
                            })}
                        >
                            <span>{suggestion.description}</span>
                        </div>
                        );
                    })}
                </div>
            </>
        )}
      </PlacesAutocomplete>
    );
}

export default LocationSearchInput