import React from 'react'

function Service(props){
    let selectedClass = props.added ? "success" : ""
    return <div class="h5-car-filter-item mix bmw" onClick={props.onClick}>
        <div class="content-area">
            <div class="price-area" style={{justifyContent: "space-between"}}>
                <div style={{ width: 'auto'}}>
                    <i className={`fa-2x ${props.added ? "far fa-check-square text-success": ""}`}></i>
                </div>
                <div style={{ width: 'auto'}}>
                    <i className={`fa-2x ${props.icon}`}></i>
                </div>
            </div>
            {/* <div class="padding-top-30">
                <img className="img-thumbnail img-fluid" src={`/images/services/${props.image ?? 'tools.webp'}`} alt="img" />
            </div> */}
            <h5 class="title padding-20">{props.name}</h5>

            {/* <ul class="list-item-wrap padding-30">
                <li class="list-items"><span class="icon flaticon-car-5"></span> Doors: 4 </li>
                <li class="list-items"><span class="icon flaticon-safety-seat"></span> Seat: 4
                    adults </li>
                <li class="list-items"><span class="icon flaticon-luggage"></span> Luggage: 2
                </li>
                <li class="list-items"><span class="icon flaticon-safety-seat"></span> No
                    Smoking</li>
            </ul> */}
            <div class="main-btn-wrap text-center margin-top-40">
                <button class={`main-btn black-border ${selectedClass}`}>{props.callOnly ? "CALL" : `SELECT${props.added ? "ED" : ""}`}</button>
            </div>
        </div>
    </div>
}

Service.defaultProps = {
    added: false,

}

export default Service