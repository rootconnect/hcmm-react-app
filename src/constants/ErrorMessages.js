export const errorMessages = {
    email: (value) => {
        return !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value) ? "Email is not in correct format" : false
    },
    phone: (value) => {
        return !/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(value) ? "Phone number is not in correct format" : false
    }
}