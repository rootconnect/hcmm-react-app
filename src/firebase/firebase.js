import firebase from 'firebase/app'
import 'firebase/database';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyDjQAcHKIBLHISyMM6ssvyBaxKaOdZDh_w",
    authDomain: "hamiltoncentralmechanic.firebaseapp.com",
    databaseURL: "https://hamiltoncentralmechanic-default-rtdb.firebaseio.com",
    projectId: "hamiltoncentralmechanic",
    storageBucket: "hamiltoncentralmechanic.appspot.com",
    messagingSenderId: "1073719059310",
    appId: "1:1073719059310:web:417cd4d6e4f1177aa5577f",
    measurementId: "G-PK4ZQG1GYP"
};

firebase.initializeApp(firebaseConfig);

export const database = firebase.database()
export const auth = firebase.auth()


