import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import reportWebVitals from './reportWebVitals';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

import './index.css'

import './assets/css/flaticon.css'
import './assets/css/fontawesome.min.css'
import './assets/css/themify-icons.css'
import './assets/css/bootstrap.min.css'
import './assets/css/meanmenu.css'
import './assets/css/style.css'
import './assets/css/responsive.css'

import 'jquery/dist/jquery'
import 'popper.js/dist/popper';
// import './assets/js/jquery-migrate.min.js'
import 'bootstrap/dist/js/bootstrap'
import './assets/js/main'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
