import * as actionTypes from '../constants/ActionTypes'

const authInitialState = {
    isAuthenticated: false
};


const auth = (state = authInitialState, action) => {
    switch (action.type) {
        case actionTypes.LOGIN:
            return {
                ...state,
                email: action.email,
                token: action.token,
                isAuthenticated: true
            }
        case actionTypes.LOGOUT:
            return {
                ...authInitialState
            }
        default:
            return state

    }
}

export { authInitialState }

export default auth