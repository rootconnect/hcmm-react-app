import * as actionTypes from '../constants/ActionTypes'

const cartInitialState = {
    services: {},
    contactDetails: {
        firstName: "",
        lastName: "",
        email: "",
        phone: "",
        make: "",
        model: "",
        color: "",
        serviceLocation: null
    },
    booking: {
        date: null,
        slot: {
            startTime: null,
            endTime: null
        },
    },
    currentStep: 1,
    error: null
};


const cart = (state = cartInitialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_SERVICE:
            return {
                ...state,
                services: {
                    ...state.services,
                    [action.service.id]: {
                        ...action.service
                    }
                }
            }
        case actionTypes.REMOVE_SERVICE:
            let services = {...state.services}

            delete services[action.serviceId]
            return {
                ...state,
                services
            }
        case actionTypes.NEXT_PAGE:

            return {
                ...state,
                currentStep: state.currentStep + 1
            }
        case actionTypes.SET_PAGE:

            return {
                ...state,
                currentStep: action.step
            }
        case actionTypes.PREVIOUS_PAGE:

            return {
                ...state,
                currentStep: state.currentStep - 1
            }
        case actionTypes.MODIFY_CONTACT_DETAILS:

            return {
                ...state,
                contactDetails: {...action.contactDetails}
            }
        case actionTypes.SET_BOOKING_DATE:
            return {
                ...state,
                booking: {
                    ...state.booking,
                    date: action.date
                }
            }
        case actionTypes.SET_BOOKING_TIME:
            return {
                ...state,
                booking: {
                    ...state.booking,
                    slot: {...action.slot}
                }
            }
        case actionTypes.RESET_BOOKING_TIME:
            return {
                ...state,
                booking: {
                    ...state.booking,
                    slot: {...cartInitialState.booking.slot}
                }
            }
        case actionTypes.CLEAR_CART:
            let st = {...cartInitialState}

            if(action.disabled){
                st.disabled = true
            }

            return st
        case actionTypes.CART_ERROR: 
            return {
                ...state,
                error: true
            }
        default:
            return state

    }
}

export { cartInitialState }

export default cart