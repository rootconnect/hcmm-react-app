import combineReducers from 'react-combine-reducers'
import authReducer, { authInitialState } from './auth'
import layoutReducer, { layoutInitialState } from './layout'
import serviceReducer, { serviceInitialState } from './service'
import cartReducer, { cartInitialState } from './cart'

const [reducerCombined, initialState] = combineReducers({
    auth: [authReducer, authInitialState],
    layout: [layoutReducer, layoutInitialState],
    service: [serviceReducer, serviceInitialState],
    cart: [cartReducer, cartInitialState],
});

export { initialState }

export default reducerCombined