import * as actionTypes from '../constants/ActionTypes'

const layoutInitialState = {
    isLoading: true
};


const layout = (state = layoutInitialState, action) => {
    switch (action.type) {
        case actionTypes.LOADING:
            return {
                ...state,
                isLoading: true
            }
        case actionTypes.LOADED:
            return {
                ...state,
                isLoading: false
            }
        default:
            return state

    }
}

export { layoutInitialState }

export default layout