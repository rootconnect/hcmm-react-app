import * as actionTypes from '../constants/ActionTypes'

const serviceInitialState = {
    items: [],
    servicesLoaded: false,
    selected: {
        options: {}
    }
};


const service = (state = serviceInitialState, action) => {
    switch (action.type) {
        case actionTypes.LOAD_SERVICES:
            return {
                ...state,
                servicesLoaded: true,
                items: [...Object.keys(action.services).map(id => ({...action.services[id], id}))]
            }
        case actionTypes.SELECT_SERVICE:
            return {
                ...state,
                selected: action.service
            }
        // case actionTypes.ADD_SERVICE:
        //     return {
        //         ...state,
        //         selected: serviceInitialState.selected
        //     }
        case actionTypes.DESELECT_SERVICE:
            return {
                ...state,
                selected: serviceInitialState.selected
            }
        default:
            return state

    }
}

export { serviceInitialState }

export default service