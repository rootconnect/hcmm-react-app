import * as firebase from '../firebase/firebase'

export const apiCall = async(url, options) => {
    let token = await firebase.auth.currentUser.getIdToken()
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }

    const opt = {
        ...options,
        body:JSON.stringify(options.body),
        headers
    }

    return fetch(`${process.env.REACT_APP_API_BASE_URL}/${url}`, opt)
    .then(
        response => (response.ok ?
            response.json() :
            Promise.reject(response)
        ),
        error => error
    )
    .then(
        json => ({ response: json }),
        error => ({ error }))
    .catch(error => { 

        if (error.name === 'AbortError') {
            console.log("Request aborted");
            return;
        }
        return {error} 
    });
}